using System;
using System.Net.Http;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using PltLibUiApp.Extensions;
using PltUiService.Extensions;
using TmsV5c.Client.Services;

namespace TmsV5c.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.Services.AddPltLibUi(ServiceLifetime.Singleton);

            ConfigureCommonServices(builder.Services, builder.Configuration["TmsUiApi:Url"]);

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services.AddAuthorizationCore();

            builder.Services.AddSingleton<IAuthorizationPolicyProvider, DefaultAuthorizationPolicyProvider>();
            builder.Services.AddSingleton<IAuthorizationService, DefaultAuthorizationService>();

            await builder.Build().RunAsync();
        }

        public static void ConfigureCommonServices(IServiceCollection services, string apiUrl)
        {
            services.AddTmsUiApiService<AuthorizationTokenService>(apiUrl);
            services.AddScoped<AuthenticationStateProvider, HostAuthenticationStateProvider>();
        }
    }
}
