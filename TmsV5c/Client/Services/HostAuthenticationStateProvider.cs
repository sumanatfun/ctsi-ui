using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using PltLibServer.Constants;
using PltUiModel.Responses;

namespace TmsV5c.Client.Services
{
    public class HostAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private ClaimsPrincipal _claimsPrincipal;

        public HostAuthenticationStateProvider(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            if (_claimsPrincipal?.Identity?.IsAuthenticated == true)
            {
                return new AuthenticationState(_claimsPrincipal);
            }

            try
            {
                const string route = "api/account/user";
                var requestUri = $"{_configuration["TMSV5C:Url"]}/{route}";
                
                var claims = await _httpClient.GetFromJsonAsync<IList<UserClaimResponse>>(requestUri);
                if (claims?.Count > 0)
                {
                    _claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(claims.Select(c => new Claim(c.Type, c.Value, c.ValueType, c.Issuer, c.OriginalIssuer)), AuthenticationConstants.AuthenticationScheme));
                    return new AuthenticationState(_claimsPrincipal);
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return new AuthenticationState(new ClaimsPrincipal());
        }
    }
}