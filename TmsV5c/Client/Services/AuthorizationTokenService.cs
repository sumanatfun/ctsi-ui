﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PltUiModel.Responses;
using PltUiService.Services.Interfaces;

namespace TmsV5c.Client.Services
{
    public class AuthorizationTokenService : IAuthorizationTokenService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        private TokenResponse _response;

        public AuthorizationTokenService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }

        public async Task<string> GetTokenAsync()
        {
            if (_response?.ExpiresAt > DateTime.Now)
            {
                return _response?.AccessToken;
            }

            try
            {
                const string route = "api/account/token";
                var requestUri = $"{_configuration["TMSV5C:Url"]}/{route}";

                _response = await _httpClient.GetFromJsonAsync<TokenResponse>(requestUri);
                return _response?.AccessToken;
            }
            catch
            {
                return null;
            }
        }
    }
}