﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PltUiModel.Responses;

namespace TmsV5c.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger) => _logger = logger;

        [HttpGet("User")]
        public IActionResult GetUser()
        {
            try
            {
                if (User?.Identity?.IsAuthenticated != true)
                {
                    return Unauthorized();
                }

                return Ok(User.Claims.Select(c => new UserClaimResponse
                {
                    Type = c.Type,
                    Value = c.Value,
                    ValueType = c.ValueType,
                    Issuer = c.Issuer,
                    OriginalIssuer = c.OriginalIssuer,
                    Properties = c.Properties
                }));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Unable to get user");
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }

        [HttpGet("Token")]
        public async Task<IActionResult> GetTokenAsync()
        {
            try
            {
                if (User?.Identity?.IsAuthenticated != true)
                {
                    return Unauthorized();
                }

                return Ok(new TokenResponse
                {
                    AccessToken = await HttpContext.GetTokenAsync("access_token"),
                    IdToken = await HttpContext.GetTokenAsync("id_token"),
                    ExpiresAt = DateTime.Parse(await HttpContext.GetTokenAsync("expires_at") ?? throw new InvalidOperationException())
                });
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Unable to get token");
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }
    }
}
