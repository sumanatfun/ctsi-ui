using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PltLibServer.Constants;
using PltLibServer.Extensions;

namespace TmsV5c.Server
{
    public class Startup
    {
        private const string BasePath = "/TMSV5C";

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddRazorPages();

            services.AddTmsServices(Configuration);
            services.AddTmsAuthentication(Configuration);

            services.AddScoped(sp =>
            {
                var httpContext = sp.GetService<IHttpContextAccessor>()?.HttpContext;

                if (httpContext == null)
                {
                    return new HttpClientHandler();
                }

                var cookies = httpContext.Request.Cookies;
                var cookieContainer = new System.Net.CookieContainer();
                foreach (var (key, value) in cookies)
                {
                    if (key.StartsWith(AuthenticationConstants.CookieName))
                    {
                        cookieContainer.Add(new System.Net.Cookie(key, value)
                        {
                            Domain = httpContext.Request.Host.Host
                        });
                    }
                }

                return new HttpClientHandler { CookieContainer = cookieContainer };
            });

            services.AddScoped(sp => new HttpClient(sp.GetService<HttpClientHandler>() ?? new HttpClientHandler()));

            Client.Program.ConfigureCommonServices(services, Configuration["TmsUiApi:Url"]);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UsePathBase(BasePath);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();

            app.UseTmsAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapFallbackToPage("/_Host").RequireAuthorization();
            });
        }
    }
}
