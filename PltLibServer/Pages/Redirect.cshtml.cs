using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace PltLibServer.Pages
{
    public class RedirectModel : PageModel
    {
        private readonly IConfiguration _configuration;

        public RedirectModel(IConfiguration configuration) => _configuration = configuration;

        public IActionResult OnGet()
        {
            if (User.Identity is not ClaimsIdentity { IsAuthenticated: true } user)
            {
                return RedirectPermanent(_configuration["TMSV5S:Url"]);
            }

            const string claimType = "https://TmsUiApi.ctsi-global.com/UserSessionContext";

            var userSessionContextClaim = user.FindFirst(claimType);
            if (userSessionContextClaim != null)
            {
                Response.Cookies.Append("UserSessionContext", userSessionContextClaim.Value);
            }

            return Page();
        }

        public IActionResult OnPostServer() => Redirect(_configuration["TMSV5S:Url"]);

        public IActionResult OnPostClient() => Redirect(_configuration["TMSV5C:Url"]);
    }
}
