using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PltLibServer.Constants;
using PltLibServer.Extensions;

namespace PltLibServer.Pages
{
    public class LoginModel : PageModel
    {
        public async Task OnGet()
        {
            var redirectUri = Request.ToAbsoluteUri("/Redirect");

            if (User?.Identity?.IsAuthenticated == true)
            {
                Response.Redirect(redirectUri);
                return;
            }

            await HttpContext.ChallengeAsync(AuthenticationConstants.AuthenticationScheme, new AuthenticationProperties
            {
                RedirectUri = redirectUri,
                IsPersistent = true,
                AllowRefresh = true
            });
        }
    }
}