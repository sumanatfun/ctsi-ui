using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PltLibServer.Constants;

namespace PltLibServer.Pages
{
    public class LogoutModel : PageModel
    {
        public async Task OnGet(string redirectUri = "/")
        {
            foreach (var (key, value) in Request.Cookies)
            {
                Response.Cookies.Delete(key, new CookieOptions
                {
                    Expires = DateTimeOffset.Now.AddDays(-1)
                });
            }

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignOutAsync(AuthenticationConstants.AuthenticationScheme, new AuthenticationProperties { RedirectUri = redirectUri });
            RedirectToRoute("/");
        }
    }
}