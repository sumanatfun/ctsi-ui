﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using PltLibServer.Constants;
using PltLibServer.Services;
using PltLibUiApp.Extensions;
using PltUiService.Extensions;

namespace PltLibServer.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTmsAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            const string loginPath = "/Login";
            const string logoutPath = "/Logout";

            services.AddDataProtection()
                .SetApplicationName("TMSV5")
                .PersistKeysToAzureBlobStorage(new Uri(configuration["Azure:BlobStorage:DataProtectionKeyUri"]));

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = _ => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.Secure = CookieSecurePolicy.Always;
            });
            // Add authentication services
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(options =>
            {
                options.Cookie.Name = AuthenticationConstants.CookieName;
                options.Cookie.SameSite = SameSiteMode.Lax;
                options.Cookie.Path = "/";
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
                options.LogoutPath = new PathString(logoutPath);
                options.LoginPath = new PathString(loginPath);

                //https://github.com/dotnet/AspNetCore.Docs/issues/21987
                options.CookieManager = new ChunkingCookieManager();
                options.TicketDataFormat = new SecureDataFormat<AuthenticationTicket>(new TicketSerializer(),
                    services.BuildServiceProvider().GetRequiredService<IDataProtectionProvider>()
                        .CreateProtector("Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationMiddleware", "Cookies.Application", "v2"));
            })
            .AddOpenIdConnect(AuthenticationConstants.AuthenticationScheme, options =>
            {
                options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;

                // Set the authority to your Auth0 domain
                options.Authority = $"https://{configuration["Auth0:Domain"]}";

                // Configure the Auth0 Client ID and Client Secret
                options.ClientId = configuration["Auth0:ClientId"];
                options.ClientSecret = configuration["Auth0:ClientSecret"];

                // Set response type to code
                options.ResponseType = OpenIdConnectResponseType.Code;

                // Configure the scope
                options.Scope.Clear();
                options.Scope.Add("openid");

                // Set the callback path, so Auth0 will call back to http://localhost:3000/callback
                // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard
                options.CallbackPath = new PathString("/callback");

                // Configure the Claims Issuer to be Auth0
                options.ClaimsIssuer = AuthenticationConstants.AuthenticationScheme;

                options.SaveTokens = true;

                options.Events = new OpenIdConnectEvents
                {
                    // handle the logout redirection
                    OnRedirectToIdentityProviderForSignOut = context =>
                    {
                        var logoutUri = $"https://{configuration["Auth0:Domain"]}/v2/logout?client_id={configuration["Auth0:ClientId"]}";

                        var postLogoutUri = context.Properties.RedirectUri;
                        if (!string.IsNullOrEmpty(postLogoutUri))
                        {
                            if (postLogoutUri.StartsWith("/"))
                            {
                                // transform to absolute
                                postLogoutUri = context.Request.ToAbsoluteUri(postLogoutUri);
                            }
                            logoutUri += $"&returnTo={Uri.EscapeDataString(postLogoutUri)}";
                        }

                        context.Response.Redirect(logoutUri);
                        context.HandleResponse();

                        return Task.CompletedTask;
                    },
                    OnRedirectToIdentityProvider = context =>
                    {
                        context.ProtocolMessage.SetParameter("audience", configuration["Auth0:Audience"]);

                        return Task.CompletedTask;
                    }
                };
            });

            services.AddHttpContextAccessor();

            return services;
        }

        public static IServiceCollection AddTmsServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTmsUiApiService<AuthorizationTokenService>(configuration["TmsUiApi:Url"]);
            services.AddPltLibUi(ServiceLifetime.Scoped);

            return services;
        }

        private static DirectoryInfo GetDirectoryInfo()
        {
            var path = Path.Combine(AppContext.BaseDirectory, "keys");
            
            return Directory.Exists(path)
                ? new DirectoryInfo(path)
                : throw new DirectoryNotFoundException(path);
        }
    }
}