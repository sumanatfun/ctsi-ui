﻿using Microsoft.AspNetCore.Http;

namespace PltLibServer.Extensions
{
    public static class RequestExtensions
    {
        public static string ToAbsoluteUri(this HttpRequest request, string relativePath) => $"{request.Scheme}://{request.Host}{request.PathBase}{relativePath}";
    }
}