﻿using Microsoft.AspNetCore.Builder;

namespace PltLibServer.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseTmsAuthentication(this IApplicationBuilder app)
        {
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            return app;
        }
    }
}