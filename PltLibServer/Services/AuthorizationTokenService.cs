﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using PltUiService.Services.Interfaces;

namespace PltLibServer.Services
{
    public class AuthorizationTokenService : IAuthorizationTokenService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthorizationTokenService(IHttpContextAccessor httpContextAccessor) => _httpContextAccessor = httpContextAccessor;

        public async Task<string> GetTokenAsync()
        {
            if (_httpContextAccessor.HttpContext?.User.Identity?.IsAuthenticated != true)
            {
                return null;
            }

            //var idToken = await _httpContextAccessor.HttpContext.GetTokenAsync("id_token");
            var accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");
            var expiresAt = await _httpContextAccessor.HttpContext.GetTokenAsync("expires_at");

            return string.IsNullOrWhiteSpace(accessToken) == false
                   && string.IsNullOrWhiteSpace(expiresAt) == false
                   && DateTime.Now < DateTime.Parse(expiresAt, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind)
                ? accessToken
                : null;
        }
    }
}