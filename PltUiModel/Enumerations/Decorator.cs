﻿using System.Text.Json.Serialization;

namespace PltUiModel.Enumerations
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Decorator
    {
        Positive,
        Negative
    }
}