﻿using System.Collections.Generic;
using PltUiModel.Enumerations;

namespace PltUiModel.Responses
{
    public class WidgetResponse
    {
        public string Title { get; set; }

        public Direction Direction { get; set; }

        public IList<DataItemResponse> DataItems { get; set; }

        public string DataItemsFooterText { get; set; }

        public FooterResponse Footer { get; set; }

        public bool DisplayZeroState { get; set; }

        public string ZeroStateIcon { get; set; }

        public string ZeroStateLabel { get; set; }
    }
}