﻿using System;

namespace PltUiModel.Responses
{
    public class TokenResponse
    {
        public string IdToken { get; set; }

        public string AccessToken { get; set; }

        public DateTime ExpiresAt { get; set; }
    }
}