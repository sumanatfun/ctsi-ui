﻿using System.Text.Json.Serialization;

namespace PltUiModel.Responses
{
    public class FavoriteResponse
    {
        [JsonPropertyName("prodId")]
        public string ProductId { get; set; }
    }
}