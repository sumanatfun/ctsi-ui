﻿using System.Collections.Generic;

namespace PltUiModel.Responses
{
    public class UserDashboardResponse
    {
        public IList<FavoriteResponse> Favorites { get; set; }

        public IList<WidgetResponse> Widgets { get; set; }
    }
}