﻿namespace PltUiModel.Responses
{
    public class V2HandlersResponse
    {
        public string GetStatus { get; set; }

        public string LoginLink { get; set; }

        public int GetStatusIntervalMilliseconds { get; set; }
    }
}