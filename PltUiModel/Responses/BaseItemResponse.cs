﻿namespace PltUiModel.Responses
{
    public class BaseItemResponse
    {
        public bool IsItem { get; set; }
        public bool IsGroup { get; set; }
        public bool IsHidden { get; set; }
    }
}