﻿using System.Collections.Generic;

namespace PltUiModel.Responses
{
    public class UserClaimResponse
    {
        public string Type { get; set; }

        public string Value { get; set; }

        public string ValueType { get; set; }

        public string Issuer { get; set; }

        public string OriginalIssuer { get; set; }

        public IDictionary<string, string> Properties { get; set; }
    }
}