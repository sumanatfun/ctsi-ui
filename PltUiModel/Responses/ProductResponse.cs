﻿namespace PltUiModel.Responses
{
    public class ProductResponse
    {
        public string ProdId { get; set; }
        public string FamilyCode { get; set; }
        public string Name { get; set; }
        public string LinkTargetType { get; set; }
        public string Link { get; set; }
        public string IconName { get; set; }
    }
}