﻿namespace PltUiModel.Responses
{
    public class FooterResponse
    {
        public bool IsPromo { get; set; }

        public string Text { get; set; }

        public string Url { get; set; }
    }
}