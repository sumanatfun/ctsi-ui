﻿using System.Collections.Generic;

namespace PltUiModel.Responses
{
    public class AppLoaderUserProductAccessResponse
    {
        public IList<MenuItemResponse> MainMenuItems { get; set; }
        public IList<MenuItemResponse> TopRightMenuItems { get; set; }
        public IList<MenuItemResponse> AdminMenuItems { get; set; }
        public IDictionary<string, ProductResponse> AccessDcty { get; set; }
        public string StartupAppId { get; set; }
    }
}