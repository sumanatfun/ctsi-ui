﻿namespace PltUiModel.Responses
{
    public class UserProfileInfoResponse
    {
        public string CustomerCode { get; set; }

        public string DisplayName { get; set; }

        public string DisplayInitial { get; set; }

        public string CompanyName { get; set; }

        public string Email { get; set; }

        public string StartupAppId { get; set; }

        public string CustomLogoLink { get; set; }

        public bool ChangePassword { get; set; }
    }
}