﻿namespace PltUiModel.Responses
{
    public class UserProductAccessResponse : ProductResponse
    {
        public int RecordId { get; set; }
        public string CustomProdName { get; set; }
        public bool UseCustomProdName { get; set; }
        public string Description { get; set; }
        public object LinkRaw { get; set; }
        public string ImageLink { get; set; }
        public string GroupPath { get; set; }
        public string ProdInfoCode { get; set; }
        public int ProdOrdinal { get; set; }
        public string CustNo { get; set; }
        public bool IsStartupApp { get; set; }
        public bool IsHidden { get; set; }
        public string Tags { get; set; }
    }
}