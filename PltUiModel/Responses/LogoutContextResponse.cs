﻿namespace PltUiModel.Responses
{
    public class LogoutContextResponse
    {
        public string PreRedirectEvent { get; set; }

        public string LogoutRedirectLink { get; set; }
    }
}