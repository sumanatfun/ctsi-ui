﻿using PltUiModel.Enumerations;

namespace PltUiModel.Responses
{
    public class DataItemResponse
    {
        public string Label { get; set; }

        public string SubLabel { get; set; }

        public string Value { get; set; }

        public Decorator Decorator { get; set; }
    }
}