﻿using System.Collections.Generic;

namespace PltUiModel.Responses
{
    public class MenuItemResponse : BaseItemResponse
    {
        public GroupPathInfoResponse GroupPathInfo { get; set; }

        public UserProductAccessResponse UserProductAccess { get; set; }
        
        public IList<MenuItemResponse> Groups { get; set; }
    }
}