﻿namespace PltUiModel.Responses
{
    public class StartupContextAppLoaderResponse
    {
        public UserProfileInfoResponse UserProfileInfo { get; set; }

        public LogoutContextResponse LogoutContext { get; set; }

        public V2HandlersResponse V2Handlers { get; set; }
    }
}