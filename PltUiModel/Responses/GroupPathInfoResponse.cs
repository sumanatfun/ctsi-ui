﻿using System.Collections.Generic;

namespace PltUiModel.Responses
{
    public class GroupPathInfoResponse
    {
        public string GroupPathMenuType { get; set; }
        public string RawGroupPath { get; set; }
        public string CleanedGroupPath { get; set; }
        public IList<LevelInfoResponse> LevelInfos { get; set; }
    }
}