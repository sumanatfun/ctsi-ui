﻿namespace PltUiModel.Responses
{
    public class LevelInfoResponse
    {
        public string GroupPath { get; set; }
        public string GroupName { get; set; }
        public int GroupDepth { get; set; }
        public int SuggestedOrdinal { get; set; }
    }
}