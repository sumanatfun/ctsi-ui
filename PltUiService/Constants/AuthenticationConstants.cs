﻿namespace PltLibServer.Constants
{
    public static class AuthenticationConstants
    {
        public const string AuthenticationScheme = "Auth0";

        public const string CookieName = ".AspNet.SharedCookie";
    }
}