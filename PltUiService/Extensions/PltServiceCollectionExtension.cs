﻿using System;
using Microsoft.Extensions.DependencyInjection;
using PltUiService.Services;
using PltUiService.Services.Interfaces;
using Polly;
using Polly.Timeout;

namespace PltUiService.Extensions
{
    public static class PltServiceCollectionExtension
    {
        public static IServiceCollection AddTmsUiApiService<T>(this IServiceCollection serviceCollection, string apiUrl) where T : IAuthorizationTokenService
        {
            serviceCollection.AddHttpClient<ITmsUiApiService, TmsUiApiService>(c => c.BaseAddress = new Uri(apiUrl))
                .AddTransientHttpErrorPolicy(b => b.WaitAndRetryAsync(3, count => TimeSpan.FromSeconds(count * 2)).WrapAsync(Policy.TimeoutAsync(30, TimeoutStrategy.Pessimistic)))
                .AddTransientHttpErrorPolicy(b => b.CircuitBreakerAsync(10, TimeSpan.FromSeconds(30)));

            serviceCollection.Add<IAuthorizationTokenService, T>();
            serviceCollection.Add<IAppContextService, AppContextService>();
            serviceCollection.Add<IAppLoaderService, AppLoaderService>();
            serviceCollection.Add<IDashboardService, DashboardService>();
            serviceCollection.Add<IAnnouncementService, AnnouncementService>();

            return serviceCollection;
        }

        private static void Add<T, TU>(this IServiceCollection serviceCollection) where TU : T
            => serviceCollection.Add(new ServiceDescriptor(typeof(T), typeof(TU), ServiceLifetime.Scoped));
    }
}