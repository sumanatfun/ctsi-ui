﻿using System.Threading.Tasks;
using PltUiModel.Responses;
using PltUiService.Services.Interfaces;

namespace PltUiService.Services
{
    public class AppContextService : IAppContextService
    {
        private readonly ITmsUiApiService _tmsUiApiService;

        public AppContextService(ITmsUiApiService tmsUiApiService) => _tmsUiApiService = tmsUiApiService;

        public async Task<StartupContextAppLoaderResponse> GetStartupContextAppLoaderAsync() => await _tmsUiApiService.GetAsync<StartupContextAppLoaderResponse>("UiAppContext/GetStartupContext/AppLoader");
    }
}