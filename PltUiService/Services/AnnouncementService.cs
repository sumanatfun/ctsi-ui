﻿using System.Threading.Tasks;
using PltUiModel.Responses;
using PltUiService.Services.Interfaces;

namespace PltUiService.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly ITmsUiApiService _tmsUiApiService;

        public AnnouncementService(ITmsUiApiService tmsUiApiService) => _tmsUiApiService = tmsUiApiService;

        public async Task<AppAnnouncementResponse> GetAppAnnouncementsAsync() => await _tmsUiApiService.GetAsync<AppAnnouncementResponse>("UiAppAnnouncement/GetUserAnnoucements");
    }
}