﻿using System.Threading.Tasks;
using PltUiModel.Responses;
using PltUiService.Services.Interfaces;

namespace PltUiService.Services
{
    public class AppLoaderService : IAppLoaderService
    {
        private readonly ITmsUiApiService _tmsUiApiService;

        public AppLoaderService(ITmsUiApiService tmsUiApiService) => _tmsUiApiService = tmsUiApiService;

        public async Task<AppLoaderUserProductAccessResponse> GetUserProductAccessAsync() => await _tmsUiApiService.GetAsync<AppLoaderUserProductAccessResponse>("UiAppLoader/GetUserProductAccess");
    }
}