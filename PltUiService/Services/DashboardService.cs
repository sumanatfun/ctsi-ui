﻿using System.Threading.Tasks;
using PltUiModel.Responses;
using PltUiService.Services.Interfaces;

namespace PltUiService.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly ITmsUiApiService _tmsUiApiService;

        public DashboardService(ITmsUiApiService tmsUiApiService) => _tmsUiApiService = tmsUiApiService;

        public async Task<UserDashboardResponse> GetUserDashboardAsync() => await _tmsUiApiService.GetAsync<UserDashboardResponse>("UiDashboard/GetUserDashboard");
    }
}