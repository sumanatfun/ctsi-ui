﻿using System.Threading.Tasks;
using PltUiModel.Responses;

namespace PltUiService.Services.Interfaces
{
    public interface IAppContextService
    {
        Task<StartupContextAppLoaderResponse> GetStartupContextAppLoaderAsync();
    }
}