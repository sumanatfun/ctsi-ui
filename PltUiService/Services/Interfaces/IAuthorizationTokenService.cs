﻿using System.Threading.Tasks;

namespace PltUiService.Services.Interfaces
{
    public interface IAuthorizationTokenService
    {
        Task<string> GetTokenAsync();
    }
}