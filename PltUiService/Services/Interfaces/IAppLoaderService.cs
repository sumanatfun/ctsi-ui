﻿using System.Threading.Tasks;
using PltUiModel.Responses;

namespace PltUiService.Services.Interfaces
{
    public interface IAppLoaderService
    {
        Task<AppLoaderUserProductAccessResponse> GetUserProductAccessAsync();
    }
}