﻿using System.Threading.Tasks;
using PltUiModel.Responses;

namespace PltUiService.Services.Interfaces
{
    public interface IDashboardService
    {
        Task<UserDashboardResponse> GetUserDashboardAsync();
    }
}