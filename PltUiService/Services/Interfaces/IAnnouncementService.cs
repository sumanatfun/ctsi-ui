﻿using System.Threading.Tasks;
using PltUiModel.Responses;

namespace PltUiService.Services.Interfaces
{
    public interface IAnnouncementService
    {
        Task<AppAnnouncementResponse> GetAppAnnouncementsAsync();
    }
}