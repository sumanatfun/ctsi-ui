﻿using System.Net.Http;
using System.Threading.Tasks;

namespace PltUiService.Services.Interfaces
{
    public interface ITmsUiApiService
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);

        Task<T> GetAsync<T>(string requestUri);
    }
}