﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PltUiService.Services.Interfaces;

namespace PltUiService.Services
{
    public class TmsUiApiService : ITmsUiApiService
    {
        private readonly HttpClient _httpClient;
        private readonly IAuthorizationTokenService _authorizationTokenService;

        public TmsUiApiService(HttpClient httpClient, IAuthorizationTokenService authorizationTokenService)
        {
            _httpClient = httpClient;
            _authorizationTokenService = authorizationTokenService;
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            var token = await _authorizationTokenService.GetTokenAsync();

            if (string.IsNullOrWhiteSpace(token))
            {
                return null;
            }

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var httpResponseMessage = await _httpClient.SendAsync(request);

            return httpResponseMessage.EnsureSuccessStatusCode();
        }

        public async Task<T> GetAsync<T>(string requestUri)
        {
            var httpResponseMessage = await SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri));

            return httpResponseMessage == null
                ? default
                : JsonConvert.DeserializeObject<T>(await httpResponseMessage.Content.ReadAsStringAsync());
        }
    }
}