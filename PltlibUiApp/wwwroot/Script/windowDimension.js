﻿// Use the module syntax to export the function
export function getWindowDimensions() {
    return {
        width: window.innerWidth,
        height: window.innerHeight
    };
}