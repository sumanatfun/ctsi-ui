﻿namespace PltLibUiApp.Models
{
    public class MenuItemModel
    {
        public string Text { get; set; }

        public string Link { get; set; }

        public string Icon { get; set; }
    }
}