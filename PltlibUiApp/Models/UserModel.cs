﻿namespace PltLibUiApp.Models
{
    public class UserModel
    {
        public string DisplayName { get; set; }

        public string Email { get; set; }

        public string DisplayInitial { get; set; }

        public string CustomerCode { get; set; }
    }
}