﻿using System.Collections.Generic;

namespace PltLibUiApp.Models
{
    public class AppBarModel
    {
        public UserModel User { get; set; }

        public IEnumerable<MenuItemModel> MenuItems { get; set; }

    }
}