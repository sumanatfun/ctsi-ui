﻿using Microsoft.Extensions.DependencyInjection;
using MudBlazor.Services;
using PltLibUiApp.Interops;
using PltLibUiApp.Interops.Interfaces;

namespace PltLibUiApp.Extensions
{
    public static class PltLibUiAppServiceCollectionExtension
    {
        /// <summary>
        /// Add UI library
        /// </summary>
        /// <param name="services"></param>
        /// <param name="lifetime">Should be Scoped coming from Server and Singleton for Client to support JS interop
        /// https://docs.microsoft.com/en-us/aspnet/core/blazor/fundamentals/dependency-injection?view=aspnetcore-5.0</param>
        /// <returns></returns>
        public static IServiceCollection AddPltLibUi(this IServiceCollection services, ServiceLifetime lifetime)
        {
            services.AddMudServices();

            services.Add<IWindowDimensionJsInterop, WindowDimensionJsInterop>(lifetime);

            return services;
        }

        private static void Add<T, TU>(this IServiceCollection services, ServiceLifetime lifetime) where TU : T => services.Add(new ServiceDescriptor(typeof(T), typeof(TU), lifetime));
    }
}