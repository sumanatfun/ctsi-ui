﻿using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using PltLibUiApp.Interops.Interfaces;
using PltLibUiApp.Models;
using PltLibUiApp.Shared;

namespace PltLibUiApp.Interops
{
    public class WindowDimensionJsInterop : IWindowDimensionJsInterop
    {
        private readonly Lazy<Task<IJSObjectReference>> _moduleTask;

        public WindowDimensionJsInterop(IJSRuntime jsRuntime) => _moduleTask = new Lazy<Task<IJSObjectReference>>(() => jsRuntime.InvokeAsync<IJSObjectReference>("import", FilePaths.WindowDimension).AsTask());

        public async Task<WindowDimension> GetAsync()
        {
            var module = await _moduleTask.Value;
            return await module.InvokeAsync<WindowDimension>("getWindowDimensions");
        }

        public async ValueTask DisposeAsync()
        {
            if (_moduleTask.IsValueCreated)
            {
                var module = await _moduleTask.Value;
                await module.DisposeAsync();
            }
        }
    }
}