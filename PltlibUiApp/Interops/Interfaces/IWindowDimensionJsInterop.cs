﻿using System;
using System.Threading.Tasks;
using PltLibUiApp.Models;

namespace PltLibUiApp.Interops.Interfaces
{
    public interface IWindowDimensionJsInterop : IAsyncDisposable
    {
        Task<WindowDimension> GetAsync();
    }
}