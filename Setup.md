# Set up Instructions #

- The app is configured to Run in IIS and not ISS express due to the difference in how both handles the base path. 
- The app runs locally as https://local-portal.ctsi-global.com domain by mapping local host to local-portal.ctsi-global.com


## Configure local-portal.ctsi-global.com ##
- 


## Configure IIS for HTTPS ##
- Run in Powershell 
	- New-SelfSignedCertificate -CertStoreLocation Cert:\LocalMachine\My -DnsName "local-portal.ctsi-global.com" -FriendlyName "local-portal.ctsi-global.com" -NotAfter (Get-Date).AddYears(10)
- Use mmc to open “Certificate (Local Computer)” see the generated under  Personal/Certificates and Copy it to “Trusted Root Certification Authorities/Certificates”. 
	- Reference : https://oak.dev/2021/02/03/create-a-self-signed-certificate-on-windows-for-local-development/
- Add Site Binding in IIS for HTTPS using the certificate we just created. 
	- Reference : https://www.tutorialsteacher.com/articles/install-ssl-certificate-in-localhost-website-iis
	



