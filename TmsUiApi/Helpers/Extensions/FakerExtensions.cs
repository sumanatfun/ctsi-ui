﻿using Bogus;

namespace TmsUiApi.Helpers.Extensions
{
    public static class FakerExtensions
    {
        public static string Initial(this Person person) => $"{person.FirstName[0]}{person.LastName[0]}";
    }
}