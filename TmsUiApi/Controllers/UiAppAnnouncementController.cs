﻿using System;
using System.Net.Mime;
using AutoBogus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PltUiModel.Responses;

namespace TmsUiApi.Controllers
{
    /// <summary>
    /// Controller for Announcement
    /// </summary>
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class UiAppAnnouncementController : ControllerBase
    {
        private readonly ILogger<UiAppAnnouncementController> _logger;

        public UiAppAnnouncementController(ILogger<UiAppAnnouncementController> logger) => _logger = logger;

        private static AppAnnouncementResponse GetResponse()
        {
            var faker = new AutoFaker<AppAnnouncementResponse>();

            return faker.Generate();
        }

        /// <summary>
        /// Gets user Announcements
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserAnnoucements")]
        [ProducesResponseType(typeof(AppAnnouncementResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppLoader()
        {
            try
            {
                return Ok(GetResponse());
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                return Problem(exception.Message);
            }
        }
    }
}
