﻿using System;
using System.Linq;
using System.Net.Mime;
using AutoBogus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PltUiModel.Responses;

namespace TmsUiApi.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UiAppLoaderController : ControllerBase
    {
        private readonly ILogger<UiAppContextController> _logger;

        public UiAppLoaderController(ILogger<UiAppContextController> logger) => _logger = logger;

        private static AppLoaderUserProductAccessResponse GetResponse()
        {
            var menuItemsFaker = new AutoFaker<MenuItemResponse>()
                .GenerateLazy(10)
                .ToArray();
            var accessDctyFaker = new AutoFaker<ProductResponse>()
                .GenerateLazy(50)
                .GroupBy(x => x.ProdId)
                .ToDictionary(k => k.Key, v => v.First());
            var appLoaderFaker = new AutoFaker<AppLoaderUserProductAccessResponse>()
                .RuleFor(x => x.AccessDcty, accessDctyFaker)
                .RuleFor(x => x.AdminMenuItems, menuItemsFaker)
                .RuleFor(x => x.MainMenuItems, menuItemsFaker)
                .RuleFor(x => x.TopRightMenuItems, menuItemsFaker);

            return appLoaderFaker.Generate();
        }

        [HttpGet]
        [Route("GetUserProductAccess")]
        [ProducesResponseType(typeof(AppLoaderUserProductAccessResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult GetUserProductAccess()
        {
            try
            {
                return Ok(GetResponse());
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                return Problem(exception.Message);
            }
        }
    }
}
