﻿using System;
using System.Linq;
using System.Net.Mime;
using AutoBogus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PltUiModel.Responses;

namespace TmsUiApi.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UiDashboardController : ControllerBase
    {
        private readonly ILogger<UiDashboardController> _logger;

        public UiDashboardController(ILogger<UiDashboardController> logger) => _logger = logger;

        private static UserDashboardResponse GetResponse()
        {
            var footerFaker = new AutoFaker<FooterResponse>()
                .RuleFor(x => x.Url, f => f.Internet.Url())
                .RuleFor(x => x.Text, f => f.Lorem.Word());
            var dateItemsFaker = new AutoFaker<DataItemResponse>()
                .RuleFor(x => x.Label, f => $"#{f.Random.Number(1000, 10000000)}")
                .RuleFor(x => x.SubLabel, f => f.Date.Past().ToString("MMM dd, yyyy"))
                .RuleFor(x => x.Value, f => f.Commerce.Price(0, 100000, 0, "$"))
                .GenerateLazy(3)
                .ToArray();
            var widgetsFaker = new AutoFaker<WidgetResponse>()
                .RuleFor(fake => fake.Title, fake => fake.Lorem.Word())
                .RuleFor(fake => fake.DataItemsFooterText, fake => fake.Lorem.Sentence())
                .RuleFor(fake => fake.ZeroStateIcon, fake => fake.Image.LoremFlickrUrl())
                .RuleFor(fake => fake.ZeroStateLabel, fake => fake.Lorem.Word())
                .RuleFor(x => x.Footer, footerFaker)
                .RuleFor(x => x.DataItems, dateItemsFaker)
                .GenerateLazy(4)
                .ToArray();
            var favoritesFaker = new AutoFaker<FavoriteResponse>()
                .RuleFor(x => x.ProductId, fake => fake.Commerce.Product())
                .GenerateLazy(5)
                .ToArray();
            var userDashboardFaker = new AutoFaker<UserDashboardResponse>()
                .RuleFor(x => x.Favorites, favoritesFaker)
                .RuleFor(x => x.Widgets, widgetsFaker);

            return userDashboardFaker.Generate();
        }

        [HttpGet]
        [Route("GetUserDashboard")]
        [ProducesResponseType(typeof(UserDashboardResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult GetUserDashboard()
        {
            try
            {
                return Ok(GetResponse());
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                return Problem(exception.Message);
            }
        }
    }
}
