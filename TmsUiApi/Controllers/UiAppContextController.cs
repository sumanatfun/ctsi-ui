﻿using System;
using System.Net.Mime;
using AutoBogus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PltUiModel.Responses;
using TmsUiApi.Helpers.Extensions;

namespace TmsUiApi.Controllers
{
    /// <summary>
    /// Controller for AppContext
    /// </summary>
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class UiAppContextController : ControllerBase
    {
        private readonly ILogger<UiAppContextController> _logger;

        public UiAppContextController(ILogger<UiAppContextController> logger) => _logger = logger;

        private static StartupContextAppLoaderResponse GetResponse()
        {
            var userProfileInfoFaker = new AutoFaker<UserProfileInfoResponse>()
                .RuleFor(fake => fake.CustomerCode, fake => fake.Random.Number(100, 999).ToString())
                .RuleFor(fake => fake.DisplayName, fake => fake.Person.FullName)
                .RuleFor(fake => fake.DisplayInitial, fake => fake.Person.Initial())
                .RuleFor(fake => fake.CompanyName, fake => fake.Company.CompanyName())
                .RuleFor(fake => fake.Email, fake => fake.Person.Email)
                .RuleFor(fake => fake.StartupAppId, fake => fake.Random.String2(10))
                .RuleFor(fake => fake.CustomLogoLink, fake => fake.Internet.Url());
            var logoutContextFaker = new AutoFaker<LogoutContextResponse>()
                .RuleFor(x => x.PreRedirectEvent, fake => fake.Lorem.Sentence())
                .RuleFor(x => x.LogoutRedirectLink, fake => fake.Internet.UrlRootedPath());
            var v2HandlersFaker = new AutoFaker<V2HandlersResponse>()
                .RuleFor(x => x.GetStatusIntervalMilliseconds, fake => fake.Random.Number(0, 1000000))
                .RuleFor(x => x.GetStatus, fake => fake.Internet.UrlRootedPath())
                .RuleFor(x => x.LoginLink, fake => fake.Internet.UrlRootedPath());
            var appLoaderFaker = new AutoFaker<StartupContextAppLoaderResponse>()
                .RuleFor(x => x.LogoutContext, logoutContextFaker)
                .RuleFor(x => x.V2Handlers, v2HandlersFaker)
                .RuleFor(x => x.UserProfileInfo, userProfileInfoFaker);

            return appLoaderFaker.Generate();
        }

        /// <summary>
        /// Gets App Loader
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStartupContext/AppLoader")]
        [ProducesResponseType(typeof(StartupContextAppLoaderResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public IActionResult GetAppLoader()
        {
            try
            {
                return Ok(GetResponse());
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                return Problem(exception.Message);
            }
        }
    }
}
